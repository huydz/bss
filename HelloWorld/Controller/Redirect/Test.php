<?php

namespace Bss\HelloWorld\Controller\Redirect;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Context;

class Test extends Action
{
    protected $resultRedirect;

    public function __construct(
        Context $context,
        \Magento\Framework\Controller\ResultFactory $result){
       $this->resultRedirect = $result;
       parent::__construct($context);

    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
        

        // $resultRedirect->setUrl('/magento2');
        $resultRedirect->setPath('/magento2');
        // 
        return $resultRedirect;    
    }  
    
}
