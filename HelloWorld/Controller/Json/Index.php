<?php

namespace Bss\HelloWorld\Controller\Json;

use Magento\Framework\App\Action\Action;

class Index extends Action
{

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $data = [
            "ten"=>"Nguyne Tuan Huy",
            "tuoi"=>"19 tuoi",
            "ngay sinh"=>"03/08/1998",
            "thong tin mo ta"=>"Fresher Developer"
        ];
        return $resultJson->setData($data);

    }
}
