<?php

namespace Bss\HelloWorld\Controller\Forward;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\App\Action\Context;

class Index extends Action
{
    protected $resultForward;

    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\ForwardFactory $forwardFactory){
       $this->_forwardFactory = $forwardFactory;
       parent::__construct($context);

    }

    public function execute()
    {
    	//$this->_forward('INDEX','Fullpage')
        $resultForward = $this->_forwardFactory->create();
		$resultForward->setController('Index');
    	$resultForward->forward('Homepage');
    	return $resultForward;
    }  
    
}
