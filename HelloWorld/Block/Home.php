<?php
namespace Bss\HelloWorld\Block;

class Home extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Helloworld constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    protected $request;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct(
            $context
        );
        $this->scopeConfig = $scopeConfig;
    }
    public function getText(){
    	// $id = $this->getRequest()->getParam();
    	// if ($id == 'data') {
    	// 	$data = [
	    //         "ten"=>"Nguyen Tuan Huy",
	    //         "tuoi"=>"19 tuoi",
	    //         "ngay sinh"=>"03/08/1998",
	    //         "thong tin mo ta"=>"Fresher Developer"
     //    	];
    	// 	return $data;
    	// }else{
    	// 	return null;
    	// }
    	return $this->_request->getParams();
    }
}
