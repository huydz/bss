<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Bss\BCTraining178\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
          /**
           * Install messages
           */
          $data = [
              ['id' => '1', 'name' => 'huy1', 'avatar' => 'Bss_178::image/ava.jpg', 'dob' => '1998-4-4', 'description' => 'dev1'],
              ['id' => '2', 'name' => 'huy2', 'avatar' => 'Bss_178::image/ava.jpg', 'dob' => '1998-3-8', 'description' => 'dev2'],
              ['id' => '3', 'name' => 'huy3', 'avatar' => 'Bss_178::image/ava.jpg', 'dob' => '1998-12-20', 'description' => 'dev3'],
              ['id' => '4', 'name' => 'huy4', 'avatar' => 'Bss_178::image/ava.jpg', 'dob' => '1998-8-3', 'description' => 'dev4'],
              ['id' => '5', 'name' => 'huy5', 'avatar' => 'Bss_178::image/ava.jpg', 'dob' => '1998-10-10', 'description' => 'dev5']
              
          ];
          foreach ($data as $bind) {
              $setup->getConnection()
                ->insertForce($setup->getTable('internship'), $bind);
          }
    }
}